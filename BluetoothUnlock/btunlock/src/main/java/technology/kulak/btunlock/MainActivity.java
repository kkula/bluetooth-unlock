package technology.kulak.btunlock;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.KeyguardManager;
import android.app.PendingIntent;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.PowerManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;


public class MainActivity extends Activity implements View.OnClickListener {
    private ComponentName mComponentName;
    private DevicePolicyManager mDPM;
    private Button mButonAdmin;
    private Button mButtonAdminDisable;
    private Button mButtonLock;
    private LinearLayout mLayout;
    private AlarmManager mAlarmManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mComponentName = new ComponentName(this, MyAdminReceiver.class);
        mDPM = (DevicePolicyManager)getSystemService(Context.DEVICE_POLICY_SERVICE);
        mAlarmManager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);


        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);


        // lock button
        mButtonLock = (Button)findViewById(R.id.lock);
        mButonAdmin = (Button)findViewById(R.id.admin_enable);
        mButtonAdminDisable = (Button)findViewById(R.id.admin_disable);
        mLayout = (LinearLayout)findViewById(R.id.afterAdmin);

        mButonAdmin.setOnClickListener(this);
        mButtonLock.setOnClickListener(this);
        mButtonAdminDisable.setOnClickListener(this);

        beforeAdmin();
        if(mDPM.isAdminActive(mComponentName)) {
            afterAdmin();
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.admin_enable:
                Intent intent = new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
                intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, mComponentName);
                intent.putExtra(DevicePolicyManager.EXTRA_ADD_EXPLANATION, "Explain!");
                startActivityForResult(intent, 15);
                break;
            case R.id.lock:
                mDPM.lockNow();

                // unlock proof of concept
                KeyguardManager km = (KeyguardManager)getSystemService(Context.KEYGUARD_SERVICE);
                final KeyguardManager.KeyguardLock kl = km.newKeyguardLock("MyKeyguardLock");
                kl.disableKeyguard();
                PowerManager pm = (PowerManager)getSystemService(Context.POWER_SERVICE);
                PowerManager.WakeLock wakeLock = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP | PowerManager.ON_AFTER_RELEASE, "MyWakeLock");
                wakeLock.acquire();



                break;
            case R.id.admin_disable:
                mDPM.removeActiveAdmin(mComponentName);
                beforeAdmin();
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    void afterAdmin() {
        mLayout.setVisibility(View.VISIBLE);
        mButonAdmin.setVisibility(View.GONE);
    }

    void beforeAdmin() {
        mLayout.setVisibility(View.GONE);
        mButonAdmin.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == 15) {
            if(resultCode == RESULT_OK) {
                // todo: unlocking buttons
                afterAdmin();
            }
        }
    }
}
